﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox
{
    class Program
    {
        static void Main()
        {
            var data = Console.ReadLine();
            var splits = data.Split(' ');
            long sum = 0;
            long num1;
            int count = 0;
            foreach (var item in splits)
            {
                if (long.TryParse(item, out num1))
                {
                    sum += long.Parse(item);
                    count++;
                }
            }
            if (sum == 0)
            {
                Console.WriteLine("HA HA very funny");
            }
            else
            {
                Console.WriteLine("Average {0}", ((double)sum) / count);  //One way
                //Console.WriteLine($"Average {((double)sum) / splits.Count())}"); //Another way
            }
            Console.ReadKey();
        }
    }
}
